import React from "react";

const Navbar = () => {
  return (
    <nav className="navbar navbar-expand-lg navbar-dark bg-success">
      <div className="container-fluid">
        <a className="navbar-brand">Navbar</a>
      </div>
    </nav>
  );
};

export default Navbar;
