import React from "react";

export default function Modal(props) {
  return (
    <div>
      <button
        type="button"
        className="btn btn-warning mt-5"
        data-toggle="modal"
        data-target={"#" + props.modalID}
      >
        Launch Modal
      </button>

      <div
        className="modal fade"
        id={props.modalID}
        tabindex="-1"
        aria-labelledby="exampleModalLabel"
        aria-hidden="true"
        data-backdrop={props.closeonBodyClick === true ? true : "static"}
        data-keyboard="false"
      >
        <div className="modal-dialog" style={{ minWidth: props.modalWidth }}>
          <div className="modal-content">
            <div
              className="modal-header"
              style={{ backgroundColor: props.headerBackground }}
            >
              <h5 className="modal-title" id="exampleModalLabel">
                {props.title}
              </h5>
              <button
                type="button"
                className="close"
                data-dismiss="modal"
                aria-label="Close"
              >
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <div className="modal-body">{props.content}</div>
            <div className="modal-footer">
              <button
                type="button"
                className="btn btn-secondary"
                data-dismiss="modal"
              >
                Close
              </button>
              <button type="button" className="btn btn-primary">
                Save changes
              </button>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}
