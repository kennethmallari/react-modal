import React from "react";

import "./index.css";
import Navbar from "./components/Navbar";
import Modal from "./components/Modal";

export default class Main extends React.Component {
  constructor() {
    super();
  }
  render() {
    return (
      <>
        <Navbar />

        <Modal
          modalWidth={1000}
          content="This is the modal body"
          closeonBodyClick={true}
          title="Register User"
          headerBackground="tomato"
          modalID="modal1"
        />
      </>
    );
  }
}
